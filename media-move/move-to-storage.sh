#!/bin/sh

# startup script
#   echo 'export MEDIA_DOWNLOAD=# ' >> ~/.bashrc 
#   echo 'export MEDIA_STORAGE=# ' >> ~/.bashrc 
#   echo 'source #/rpi-util-scripts/media-move/move-to-storage.sh &' >> ~/.bashrc 

sleep 5m

if [[ -z "${MEDIA_DOWNLOAD}" ]]; then
  wall "Missing media download location!"
fi
  
if [[ -z "${MEDIA_STORAGE}" ]]; then
  wall "Missing media storage location!"
fi
  
wall "Starting to move download media to storage."

cp -r "${MEDIA_DOWNLOAD}/Movies/." "${MEDIA_STORAGE}/Movies"
rm -fr "${MEDIA_DOWNLOAD}/Movies/"
cp -r "${MEDIA_DOWNLOAD}/Series/." "${MEDIA_STORAGE}/Series"
rm -fr "${MEDIA_DOWNLOAD}/Series/"
cp -r "${MEDIA_DOWNLOAD}/Documentary/." "${MEDIA_STORAGE}/Documentary"
rm -fr "${MEDIA_DOWNLOAD}/Documentary/"

wall "Finished moving download media to storage."